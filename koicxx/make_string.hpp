#ifndef KOICXX_MAKE_STRING_HPP
#define KOICXX_MAKE_STRING_HPP

#include <sstream>
#include <string>

namespace koicxx {

namespace detail {

/**
 * \brief Creates a std::basic_string object using an iostream-like operator<< syntax
 */
template <typename CHAR_TYPE>
class basic_make_string
{
public:
  template <typename T>
  basic_make_string& operator<<(const T& arg)
  {
    _stream << arg;
    return *this;
  }

  operator std::basic_string<CHAR_TYPE>() const
  {
    return _stream.str();
  }

protected:
  std::basic_ostringstream<CHAR_TYPE> _stream;
};

} // namespace detail

typedef detail::basic_make_string<char>     make_string;
typedef detail::basic_make_string<wchar_t>  make_wstring;

} // namespace koicxx

#endif // !KOICXX_MAKE_STRING_HPP