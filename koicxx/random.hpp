#ifndef KOICXX_RANDOM_HPP
#define KOICXX_RANDOM_HPP

# if BOOST_VERSION < 105300
#  include <boost/thread/locks.hpp>
# else // BOOST_VERSION >= 105300
#  include <boost/thread/lock_guard.hpp>
# endif
#include <boost/thread/mutex.hpp>

#include <random>

namespace koicxx {
namespace random {

/**
 * \brief Generates a random number according to the passed arguments. It has a fully thread-safe implementation
 */
template <
  typename RngType = std::mt19937
  , template <class> class DistributionType = std::uniform_int_distribution
>
class random_number_generator
{
public:
  random_number_generator()
    : _rd()
    , _gen(_rd()) {}

  template <typename RangeType>
  typename RngType::result_type get_random_number(RangeType range_start, RangeType range_end)
  {
    boost::lock_guard<boost::mutex> lock(_sync);
    DistributionType<RangeType> dist(range_start, range_end);
    return dist(_gen);
  }

private:
  std::random_device _rd;
  RngType _gen;
  boost::mutex _sync;
};

} // namespace random
} // namespace koicxx

#endif // !KOICXX_RANDOM_HPP