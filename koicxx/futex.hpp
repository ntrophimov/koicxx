#ifndef KOICXX_FUTEX_HPP
#define KOICXX_FUTEX_HPP

# ifndef _MSC_VER
#  error This is MSVC-specific header file
# endif // !_MSC_VER

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace koicxx {

  /**
   * \brief RAII implementation for futexes
   */
  class futex
  {
  public:
    futex() : _cs(NULL)
    {
      PCRITICAL_SECTION cs = new CRITICAL_SECTION;
      InitializeCriticalSection(cs);
      PVOID old_cs = InterlockedCompareExchangePointer((void**)&_cs, cs, NULL);
      if (old_cs != NULL)
      {
        DeleteCriticalSection(cs);
        delete cs;
      }
    }

    ~futex()
    {
      PVOID old_cs = InterlockedExchangePointer((void**)&_cs, NULL);
      if (old_cs != NULL)
      {
        DeleteCriticalSection((PCRITICAL_SECTION)old_cs);
        delete old_cs;
      }
    }

    void lock()
    {
      EnterCriticalSection(_cs);
    }

    void unlock()
    {
      LeaveCriticalSection(_cs);
    }

  private:
    PCRITICAL_SECTION _cs;
  };

} // namespace koicxx

#endif // !KOICXX_FUTEX_HPP